# create symlinks in vendor folders, one does not use webpack for that
echo "Creating symlinks..."
ln -sf ../../../node_modules/bootstrap/dist/css/bootstrap.min.css public/css/vendor/bootstrap.min.css
ln -sf ../../../node_modules/marked/marked.min.js public/js/vendor/marked.min.js
ln -sf ../../../node_modules/jquery/dist/jquery.min.js public/js/vendor/jquery.min.js
