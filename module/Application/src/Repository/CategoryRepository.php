<?php

declare(strict_types=1);

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\Category;
use Application\Entity\Recipe;

class CategoryRepository extends EntityRepository
{
    public function findWithRecipies(): array
    {
        $result = $this->createQueryBuilder('c')
            ->leftJoin('c.recipies', 'r')
            ->addSelect('SUM(r.id) AS recipiesCount')
            ->groupBy('c.id')
            ->getQuery()
            ->getResult();

        return \array_map(function (array $row) {
            $entity = $row[0];

            $entity->setRecipiesCount((int) $row['recipiesCount'] ?? 0);

            return $entity;
        }, $result);
    }
}