<?php

declare(strict_types=1);

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\Category;

class RecipeRepository extends EntityRepository
{
    public function findByQuery(string $query): array
    {
        $qb = $this->createQueryBuilder('r');
        $qb
            ->join('r.category', 'c')
            ->where('r.name LIKE :query')
            ->orWhere('c.name LIKE :query')
            ->setParameter('query', "%{$query}%");

        return $qb->getQuery()->getResult();
    }
}