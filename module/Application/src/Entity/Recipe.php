<?php

declare(strict_types=1);

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Application\Repository\RecipeRepository")
 * @ORM\Table(name="recipies")
 * @ORM\HasLifecycleCallbacks
 */
class Recipe
{
    /** 
     * @var int
     *
     * @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue 
     */
    private $id;

    /** 
     * @var string
     * 
     * @ORM\Column(type="string") 
     */
    private $name;

    /** 
     * @var string
     * 
     * @ORM\Column(type="text") 
     */
    private $text;

    /**
     * @var Category
     * 
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="recipies")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /** 
     * @var \DateTime
     * 
     * @ORM\Column(type="datetime") 
     */
    private $created;

    /** 
     * @var \DateTime
     * 
     * @ORM\Column(type="datetime", nullable=true) 
     */
    private $updated;

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateTimestamps()
    {
        if (null === $this->created) {
            $this->created = new \DateTime();
        }

        $this->updated = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text): self
    {
        $this->text = $text;

        return $this;
    }
}