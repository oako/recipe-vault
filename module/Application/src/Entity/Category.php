<?php

declare(strict_types=1);

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Application\Repository\CategoryRepository")
 * @ORM\Table(name="categories")
 * @ORM\HasLifecycleCallbacks
 */
class Category
{
    /** 
     * @var int
     *
     * @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue 
     */
    private $id;

    /** 
     * @var string
     * 
     * @ORM\Column(type="string") 
     */
    private $name;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;

    /**
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="Recipe", mappedBy="category")
     * @ORM\JoinColumn(name="id", referencedColumnName="category_id")
     */
    private $recipies;

    /**
     * @var int
     */
    private $recipiesCount = 0;

    /** 
     * @var \DateTime
     * 
     * @ORM\Column(type="datetime") 
     */
    private $created;

    /** 
     * @var \DateTime
     * 
     * @ORM\Column(type="datetime", nullable=true) 
     */
    private $updated;

    public function __construct()
    {
        $this->recipies = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateTimestamps()
    {
        if (null === $this->created) {
            $this->created = new \DateTime();
        }

        $this->updated = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRecipiesCount(): int
    {
        return $this->recipiesCount;
    }

    public function setRecipiesCount(int $count): self
    {
        $this->recipiesCount = $count;

        return $this;
    }
}