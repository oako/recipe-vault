<?php

declare(strict_types=1);

namespace Application\Form;

use Zend\Form\Form;
use Doctrine\ORM\EntityManagerInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Application\Entity\Recipe;
use Application\Entity\Category;
use DoctrineModule\Form\Element\ObjectSelect;

class RecipeForm extends Form
{
    private $em;

    public function __construct(EntityManagerInterface $em, Recipe $entity)
    {
        parent::__construct('recipe', []);

        $this->em = $em;

        $hydrator = new DoctrineObject($em);

        $this
            ->add([
                'name' => 'name',
                'type' => 'text',
                'options' => [
                    'label' => 'Title',
                ],
                'attributes' => [
                    'placeholder' => 'Title',
                    'class' => 'form-control',
                ],
            ])
            ->add([
                'type' => ObjectSelect::class,
                'name' => 'category',
                'options' => [
                    'label' => 'Category',
                    'object_manager' => $this->em,
                    'target_class' => Category::class,
                    'property' => 'name',
                    'is_method' => true,
                ],
                'attributes' => [
                    'class' => 'form-control',
                ],
            ])
            ->add([
                'name' => 'text',
                'type' => 'textarea',
                'options' => [
                    'label' => 'Recipe Description',
                ],
                'attributes' => [
                    'placeholder' => 'Description',
                    'class' => 'form-control js-markdown-preview',
                    'rows' => 10,
                ],
            ])
            ->add([
                'name' => 'submit',
                'type' => 'submit',
                'attributes' => [
                    'value' => 'Add',
                    'id'    => 'submitbutton',
                    'class' => 'btn btn-primary',
                ],
            ])
            ->setHydrator($hydrator)
            ->bind($entity);
    }
}