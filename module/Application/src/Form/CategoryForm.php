<?php

declare(strict_types=1);

namespace Application\Form;

use Zend\Form\Form;
use Application\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;

class CategoryForm extends Form
{
    public function __construct(EntityManagerInterface $em, Category $entity)
    {
        parent::__construct('category', []);

        $hydrator = new DoctrineObject($em);

        $this
            ->add([
                'name' => 'name',
                'type' => 'text',
                'options' => [
                    'label' => 'Title',
                ],
                'attributes' => [
                    'class' => 'form-control',
                    'placeholder' => 'Title',
                ],
            ])
            ->add([
                'name' => 'description',
                'type' => 'text',
                'options' => [
                    'label' => 'Description',
                ],
                'attributes' => [
                    'class' => 'form-control',
                    'placeholder' => 'Description',
                ],
            ])
            ->add([
                'name' => 'submit',
                'type' => 'submit',
                'attributes' => [
                    'value' => 'Add',
                    'id'    => 'submitbutton',
                    'class' => 'btn btn-primary',
                ],
            ])
            ->setHydrator($hydrator)
            ->bind($entity);
    }
}