<?php

declare(strict_types=1);

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Doctrine\ORM\EntityManagerInterface;
use Application\Entity\Recipe;
use Application\Form\RecipeForm;
use Zend\View\Model\ViewModel;
use Application\Form\Filter\RecipeInputFilter;
use Doctrine\Common\Collections\Criteria;

class RecipeController extends AbstractActionController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function indexAction()
    {
        $repository = $this->em->getRepository(Recipe::class);
        $query = $this->params()->fromQuery('query');

        if (null !== $query && 3 <= strlen($query)) {
            $recipies = $repository->findByQuery($query);
        } else {
            $recipies = $repository->findAll();
        }

        return [
            'query' => $query,
            'recipies' => $recipies,
        ];
    }

    public function addAction()
    {
        $recipe = new Recipe();
        $form = new RecipeForm($this->em, $recipe);
        $addUrl = $this->url()->fromRoute('recipe', ['action' => 'add']);

        $form
            ->setAttribute('action', $addUrl)
            ->get('submit')
            ->setValue('Add');

        $request = $this->getRequest();

        $view = new ViewModel([
            'title' => 'Add Recipe',
            'form' => $form,
        ]);

        $view->setTemplate('application/recipe/form.phtml');

        if (!$request->isPost()) {
            return $view;
        }

        $filter = new RecipeInputFilter();

        $form->setInputFilter($filter->getInputFilter());
        $form->setData($request->getPost());

        if (!$form->isValid()) {
            return $view;
        }

        $this->em->persist($recipe);

        $this->em->flush();

        return $this->redirect()->toRoute('recipe');
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id');
        $recipe = $this->em->getRepository(Recipe::class)->findOneBy(['id' => $id]);

        if (null === $recipe) {
            return $this->redirect()->toRoute('recipe');
        }

        $form = new RecipeForm($this->em, $recipe);
        $editUrl = $this->url()->fromRoute('recipe', ['action' => 'edit', 'id' => $id]);

        $form
            ->setAttribute('action', $editUrl)
            ->get('submit')
            ->setValue('Edit');

        $request = $this->getRequest();

        $view = new ViewModel([
            'title' => 'Add Recipe',
            'form' => $form,
        ]);

        $view->setTemplate('application/recipe/form.phtml');

        if (!$request->isPost()) {
            return $view;
        }

        $filter = new RecipeInputFilter();

        $form->setInputFilter($filter->getInputFilter());
        $form->setData($request->getPost());

        if (!$form->isValid()) {
            return $view;
        }

        $this->em->persist($recipe);
        $this->em->flush();

        return $this->redirect()->toRoute('recipe');
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', null);
        $recipe = $this->em->getRepository(Recipe::class)->findOneBy(['id' => $id]);

        if (null === $recipe) {
            return $this->redirect()->toRoute('recipe');
        }

        $this->em->remove($recipe);
        $this->em->flush();

        return $this->redirect()->toRoute('recipe');
    }
}