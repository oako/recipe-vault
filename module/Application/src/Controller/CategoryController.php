<?php

declare(strict_types=1);

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Doctrine\ORM\EntityManagerInterface;
use Application\Entity\Category;
use Application\Form\CategoryForm;
use Application\Form\Filter\CategoryInputFilter;
use Zend\View\Model\ViewModel;
use DoctrineORMModule\Service\DoctrineObjectHydratorFactory;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Doctrine\Common\Util\Debug;
use Zend\Mvc\Controller\Plugin\Url;

class CategoryController extends AbstractActionController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function indexAction()
    {
        $categories = $this->em->getRepository(Category::class)->findWithRecipies();

        return [
            'categories' => $categories,
        ];
    }

    public function addAction()
    {
        $category = new Category();
        $form = new CategoryForm($this->em, $category);
        $addUrl = $this->url()->fromRoute('category', ['action' => 'add']);

        $form
            ->setAttribute('action', $addUrl)
            ->get('submit')
            ->setValue('Add');

        $request = $this->getRequest();

        $view = new ViewModel([
            'title' => 'Add Category',
            'form' => $form,
        ]);

        $view->setTemplate('application/category/form.phtml');

        if (!$request->isPost()) {
            return $view;
        }

        $filter = new CategoryInputFilter();

        $form->setInputFilter($filter->getInputFilter());
        $form->setData($request->getPost());

        if (!$form->isValid()) {
            return $view;
        }

        $this->em->persist($category);
        $this->em->flush();

        return $this->redirect()->toRoute('category');
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id');
        $category = $this->em->getRepository(Category::class)->findOneBy(['id' => $id]);

        if (null === $category) {
            return $this->redirect()->toRoute('category', ['action' => 'add']);
        }

        $form = new CategoryForm($this->em, $category);
        $request = $this->getRequest();

        if ($request->isPost()) {
            $filter = new CategoryInputFilter();
            $form->setInputFilter($filter->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->em->persist($category);
                $this->em->flush();

                return $this->redirect()->toRoute('category');
            }
        }
     
        $editUrl = $this->url()->fromRoute('category', ['action' => 'edit', 'id' => $category->getId()]);

        $form
            ->setAttribute('action', $editUrl)
            ->get('submit')
            ->setValue('Edit');

        $view = new ViewModel([
            'title' => 'Edit Category',
            'form' => $form,
        ]);

        $view->setTemplate('application/category/form.phtml');

        return $view;
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', null);
        $category = $this->em->getRepository(Category::class)->findOneBy(['id' => $id]);

        if (null === $category) {
            return $this->redirect()->toRoute('category');
        }

        $this->em->remove($category);
        $this->em->flush();

        return $this->redirect()->toRoute('category');
    }
}