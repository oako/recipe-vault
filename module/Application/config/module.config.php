<?php

declare(strict_types=1);

namespace Application;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Application\Controller\Factory\CategoryControllerFactory;
use Application\Controller\Factory\RecipeControllerFactory;

return [
    'controllers' => [
        'factories' => [
            Controller\CategoryController::class => CategoryControllerFactory::class,
            Controller\RecipeController::class => RecipeControllerFactory::class,
        ],
    ],
    'router' => require __DIR__ . '/routes.php',
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'doctrine' => [
        'driver' => [
            'recipe_driver' => [
                'class' => AnnotationDriver::class,
                // 'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    'Application\Entity' => 'recipe_driver',
                ],
            ],
        ],
    ],
];