<?php

declare(strict_types=1);

namespace Application;

use Zend\Router\Http\Segment;
use Zend\Router\Http\Literal;

return [
    'routes' => [
        'home' => [
            'type' => Literal::class,
            'options' => [
                'route' => '/',
                'defaults' => [
                    'controller' => Controller\RecipeController::class,
                    'action' => 'index',
                ],
            ],
        ],
        'category' => [
            'type' => Segment::class,
            'options' => [
                'route' => '/categories[/:action[/:id]]',
                'constraints' => [
                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    'id'     => '[0-9]+',
                ],
                'defaults' => [
                    'controller' => Controller\CategoryController::class,
                    'action' => 'index',
                ],
            ],
        ],
        'recipe' => [
            'type' => Segment::class,
            'options' => [
                'route' => '/recipies[/:action[/:id]]',
                'constraints' => [
                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    'id'     => '[0-9]+',
                ],
                'defaults' => [
                    'controller' => Controller\RecipeController::class,
                    'action' => 'index',
                ],
            ],
        ],
    ],
];