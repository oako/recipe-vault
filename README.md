# Recipe Vault

## Установка

```bash
$ composer install
$ yarn
$ docker-compose up -d
$ docker-compose run zf vendor/bin/doctrine-module migrations:migrate
```